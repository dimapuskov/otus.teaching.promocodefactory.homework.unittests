﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.UnitTests.Extensions;


namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public class TestFixtureInMemoryDb
    {
        public TestFixtureInMemoryDb()
        {
            ServiceCollection = new ServiceCollection();
            ServiceProvider =  GetServiceProvider();
        }
        public IServiceProvider ServiceProvider { get; set; }
        public IServiceCollection ServiceCollection { get; set; }

        private IServiceProvider GetServiceProvider()
        {
            var serviceProvider = ServiceCollection
                    .ConfigureInMemoryContext()
                    .BuildServiceProvider();
            SeedDataForTests.Seed(serviceProvider);
            return serviceProvider;
        }
    }
}