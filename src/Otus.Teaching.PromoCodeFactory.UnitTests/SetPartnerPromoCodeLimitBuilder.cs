﻿using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public class SetPartnerPromoCodeLimitBuilder
    {
        private int _limit;
        private DateTime _endDate;


        public SetPartnerPromoCodeLimitBuilder WithCreatedLimit(int limit)
        {
            _limit = limit;
            return this;
        }

        public SetPartnerPromoCodeLimitBuilder WithCreatedEndDate(DateTime endDate)
        {
            _endDate = endDate;
            return this;
        }

        public SetPartnerPromoCodeLimitRequest Build()
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                    Limit = _limit,
                    EndDate = _endDate
            };
        }

    }
}