﻿using System;
using System.Linq;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncInMemoryDbTests : IClassFixture<TestFixtureInMemoryDb>
    {
        private readonly IRepository<Partner> _partnerRepository;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncInMemoryDbTests(TestFixtureInMemoryDb testFixture)
        {
            var serviceProvider = testFixture.ServiceProvider;
            _partnerRepository = serviceProvider.GetService<IRepository<Partner>>();
            _partnersController = new PartnersController(_partnerRepository);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_AddedNewLimits_LimitsSavedInDatabase()
        {
            // Arrange
            var testDate = DateTime.Now;
            var testLimit = 10;

            var requestBuilder = new SetPartnerPromoCodeLimitBuilder();
            requestBuilder.WithCreatedLimit(testLimit);
            requestBuilder.WithCreatedEndDate(testDate);
            var request = requestBuilder.Build();
            
            // Act
            var setLimitResult = await _partnersController.SetPartnerPromoCodeLimitAsync(SeedDataForTests.SeedGuid, request);
            var updatedPartner = await _partnerRepository.GetByIdAsync(SeedDataForTests.SeedGuid);
            
            // Assert
            setLimitResult.Should().BeAssignableTo<CreatedAtActionResult>();
            updatedPartner.PartnerLimits.FirstOrDefault(x => x.Limit == testLimit && x.EndDate == testDate).Should().NotBeNull();
        }

    }
}