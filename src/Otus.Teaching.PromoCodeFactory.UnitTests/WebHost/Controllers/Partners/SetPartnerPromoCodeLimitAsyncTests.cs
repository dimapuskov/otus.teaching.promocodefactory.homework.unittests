﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                    {
                            new PartnerPromoCodeLimit()
                            {
                                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                                    CreateDate = new DateTime(2020, 07, 9),
                                    EndDate = new DateTime(2020, 10, 9),
                                    Limit = 100
                            }
                    }
            };

            return partner;
        }

        public SetPartnerPromoCodeLimitRequest CreateBaseRequest()
        {
            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            return request;
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;
            var request = CreateBaseRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                    .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerBlocked_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.IsActive = false;
            var request = CreateBaseRequest();


            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                    .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_RequestNull_ReturnsArgumentException()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                    .ReturnsAsync(partner);

            // Act
            Func<Task<IActionResult>> result = async () => await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);

            // Assert\
            result.Should().Throw<ArgumentNullException>().WithMessage("Reuest shouldn't be null (Parameter 'request')");
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_ReuestLimit0_ReturnsBadRequst()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            var request = CreateBaseRequest();
            request.Limit = 0;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                    .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }


        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PromocodeEndDateExpired_LimitNotErased_AddedNewLimits()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            var request = CreateBaseRequest();

            partner.NumberIssuedPromoCodes = 10;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                    .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);


            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.PartnerLimits.Count.Should().BeGreaterThan(1);
            partner.PartnerLimits?.Where(c => c.EndDate >= DateTime.Now && !c.CancelDate.HasValue).Count().Should()
                    .BeLessOrEqualTo(1);
            partner.NumberIssuedPromoCodes.Should().Be(10);

        }
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PromocodeEndDateNotExpired_LimitErased_AddedNewLimits()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            var request = CreateBaseRequest();
            partner.NumberIssuedPromoCodes = 10;
            partner.PartnerLimits.FirstOrDefault().EndDate = DateTime.Today.AddDays(1);


            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                    .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);


            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.PartnerLimits.Count.Should().BeGreaterThan(1);
            partner.PartnerLimits?.Where(c => c.EndDate >= DateTime.Now && !c.CancelDate.HasValue).Count().Should()
                    .BeLessOrEqualTo(1);
            partner.PartnerLimits.FirstOrDefault(c => c.Id == Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"))
                    .CancelDate.Should().NotBeNull();

            partner.NumberIssuedPromoCodes.Should().Be(0);

        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PromocodeHasCancelDate_LimitErased_AddedNewLimits()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            var request = CreateBaseRequest();

            partner.PartnerLimits.FirstOrDefault().CancelDate = DateTime.Now;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                    .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);


            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.PartnerLimits.Count.Should().BeGreaterThan(1);
            partner.PartnerLimits?.Where(c => c.EndDate >= DateTime.Now && !c.CancelDate.HasValue).Count().Should()
                    .BeLessOrEqualTo(1);
            partner.PartnerLimits.FirstOrDefault(c => c.Id == Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"))
                    .CancelDate.Should().NotBeNull();
            partner.NumberIssuedPromoCodes.Should().Be(0);

        }
    }
}