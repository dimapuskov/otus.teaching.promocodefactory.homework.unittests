﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public static class SeedDataForTests
    {
        public static Guid SeedGuid = Guid.NewGuid();

        public static void Seed(IServiceProvider provider)
        {
            var dbContext = provider.GetService<DataContext>(); 
            SeedPartners(dbContext);
        }

        public static void SeedPartners(DataContext context)
        {
            Partner partner = new Partner()
            {
                    Id = SeedGuid,
                    IsActive =  true,
                    Name =  "Name",
                    NumberIssuedPromoCodes = 155,
                    PartnerLimits = new List<PartnerPromoCodeLimit>()
                    {
                            new PartnerPromoCodeLimit()
                            {
                                    Id = SeedGuid,
                                    CreateDate = DateTime.Now,
                                    EndDate = DateTime.Now.AddDays(10),
                                    Limit = 10
                            }
                    }
            };

            context.Partners.Add(partner);
            context.SaveChanges();
        }
    }
}