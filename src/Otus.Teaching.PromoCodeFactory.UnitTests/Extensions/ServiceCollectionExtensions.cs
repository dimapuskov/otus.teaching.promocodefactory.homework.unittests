﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection ConfigureInMemoryContext(this IServiceCollection collection)
        {
            var serviceProvider = new ServiceCollection()
                    .AddEntityFrameworkInMemoryDatabase()
                    .BuildServiceProvider();

            collection.AddDbContext<DataContext>(c =>
            {
                c.UseInMemoryDatabase("MemoryDb", b => { });
                c.UseInternalServiceProvider(serviceProvider);
            });
            collection.AddTransient<DbContext, DataContext>();
            collection.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));

            return collection;
        }
    }
}